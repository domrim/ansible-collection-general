============================
domrim.general Release Notes
============================

.. contents:: Topics

v0.8.0
======

Release Summary
---------------

Implemented automatic publishing from gitlab-ci

v0.7.3
======

Bugfixes
--------

- age_keygen - fixed module options

v0.7.2
======

Release Summary
---------------

This is the first proper release of the ``domrim.general`` collection on 2025-02-11.
