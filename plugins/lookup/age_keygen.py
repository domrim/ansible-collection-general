# python 3 headers, required if submitting to Ansible
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r"""
  name: domrim.general.age_keygen
  author: Dominik Rimpf <dev@drimpf.de>
  short_description: create age identity
  description:
    - This lookup returns a age identity in variables 
  options: []
  notes: []
"""
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display
from pyrage import x25519


display = Display()


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        self.set_options(var_options=variables, direct=kwargs)

        ident = x25519.Identity.generate()

        return [{'public_key': ident.to_public(),
                 'private_key': str(ident)}]
