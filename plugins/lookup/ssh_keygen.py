# python 3 headers, required if submitting to Ansible
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r"""
  name: domrim.general.ssh_keygen
  author: Dominik Rimpf <dev@drimpf.de>
  short_description: create ssh keypair
  description:
    - This lookup returns a ssh keypair in variables 
  options:
    comment:
      description: SSH Key comment
      type: str
      default: ''
    type:
      description: SSH Key type
      type: string
      default: 'ed25519'
      choices: ['rsa', 'ecdsa', 'ed25519']
  notes: []
"""
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display
from ansible_collections.community.crypto.plugins.module_utils.openssh.cryptography import OpensshKeypair

display = Display()


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        self.set_options(var_options=variables, direct=kwargs)

        keypair = OpensshKeypair.generate(
            keytype=self.get_option('type'),
            comment=self.get_option('comment'),
        )

        encoded_public_key = OpensshKeypair.encode_openssh_publickey(
            keypair.asymmetric_keypair, self.get_option('comment')
        )

        encoded_private_key = OpensshKeypair.encode_openssh_privatekey(
            keypair.asymmetric_keypair, 'SSH'
        )

        return [{'public_key': encoded_public_key,
                 'private_key': encoded_private_key}]
